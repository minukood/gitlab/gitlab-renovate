# gitlab-renovate

This project allows you to easily run `renovate` within GitLab.  
For example as a scheduled task.  
(Both GitLab.com and Self-Managed)

## What is Renovate?
> ### Renovate
> Automated dependency updates. Multi-platform and multi-language.
>
> Repository: https://github.com/renovatebot/renovate  
> Documentation: https://docs.renovatebot.com  

## Usage

* Fork this project into a group
* Configure CI/CD variables on project (or preferrably group)
  * `GITLAB_API_TOKEN` GitLab API token for accessing repositories
  * `GITHUB_COM_TOKEN` Any personal user token for github.com for fetching changelogs
* Configure a scheduled run of the pipeline with an interval that suits you
* Add any addition configuration to [`renovate/config.js`](./renovate/config.js)

## What it does

* Automatic configuration of GitLab API endpoints (Works with both GitLab.com and Self-Managed)
* Autodiscover all repositories located in same GitLab main group as `gitlab-renovate` project

Ex: Given that this projects location is `gitlab.com/minukood/gitlab/gitlab-renovate`,
renovate will run against all repositories in `gitlab.com/minukood`.
