/**
 * Get environment variable
 */
function getEnv(envName) {
  const envVar = process.env[envName];

  if (!envVar) {
    throw new Error(`Environment variable ${envName} is not defined.`);
  }

  return envVar;
}

/**
 * Load GitLab group from current project
 */
function gitlabMainGroupFilter() {
  const projectPath = getEnv('CI_PROJECT_PATH');
  const mainGroupName = projectPath.split('/')[0];

  if (!mainGroupName) {
    throw new Error('Unable to find GitLab main group.');
  }

  return `${mainGroupName}/**`;
}

module.exports = {
  platform: 'gitlab',
  endpoint: getEnv('CI_API_V4_URL'),
  // Autodiscover
  autodiscover: true,
  autodiscoverFilter: gitlabMainGroupFilter(),
  // Onboarding
  onboarding: true,
  onboardingConfig: {
    extends: [
      'config:recommended',
      ':prHourlyLimitNone',
      ':prConcurrentLimitNone'
    ]
  },
  // Debug
  printConfig: false,
  dryRun: null
};
