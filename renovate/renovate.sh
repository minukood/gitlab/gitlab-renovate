#!/usr/bin/env sh

set -euo > /dev/null

usage() {
  cat << EOF
gitlab-renovate

Environment variables: (Save as group CI/CD variables)
GITLAB_API_TOKEN  GitLab API token for accessing repositories.
GITHUB_COM_TOKEN  Any personal user token for github.com for fetching changelogs.

EOF
  exit 1
}

#

# Configure GitLab API Token
if [ -z "${GITLAB_API_TOKEN:-}" ]; then
  >&2 echo "Error: missing GITLAB_API_TOKEN"
  usage
fi
export RENOVATE_TOKEN="${GITLAB_API_TOKEN}"

# Configure GitHub Token
if [ -z "${GITHUB_COM_TOKEN:-}" ]; then
  >&2 echo "Error: missing GITHUB_COM_TOKEN"
  usage
fi
export GITHUB_COM_TOKEN="${GITHUB_COM_TOKEN}"

THIS_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
RENOVATE_CONFIG_FILE="${THIS_DIR}/config.js" renovate "$@"
